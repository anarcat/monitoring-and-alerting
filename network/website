#!/usr/bin/python3

import numpy as np

from datetime import datetime, timedelta
from pandas import read_csv
from prometheus_client import CollectorRegistry, Gauge, write_to_textfile
from sklearn.linear_model import LinearRegression



registry = CollectorRegistry()


network_clients_trend = Gauge('network_clients_trend', 'Current trend for relays clients on the network per node and country',
                        ['node', 'country'], registry=registry)


series = read_csv('/srv/metrics.torproject.org/metrics/shared/stats/clients.csv', header=0, parse_dates=[0])
series = series[ series.date >= datetime.utcnow() - timedelta(days=15) ]
series = series[ series.country != "??" ]
series = series.dropna(subset=['country'])

countries = series.country.unique()

for country in countries:

    df = series[ series.country == country ]

    # for relays clients
    d_relays = df[ df.node == 'relay' ]

    if d_relays.clients.size and np.average(d_relays.clients.values) > 100:
        X = [i for i in range(0, len(d_relays))]
        X = np.reshape(X, (len(X), 1))
        value = 0
        if len(X) > 0:
            y = d_relays.clients.values
            model = LinearRegression()
            model.fit(X, y)
            trend = model.predict(X)
            value = (trend[len(trend)-1]-trend[0]) / trend[0] * 100

    network_clients_trend.labels(node='relay', country=country).set(value)

    # for bridges clients
    d_bridges = df[ df.node == 'bridge' ]

    if d_bridges.clients.size and np.average(d_bridges.clients.values) > 100:
        X = [i for i in range(0, len(d_bridges))]
        X = np.reshape(X, (len(X), 1))
        value = 0
        if len(X) > 0:
            y = d_bridges.clients.values
            model = LinearRegression()
            model.fit(X, y)
            trend = model.predict(X)
            value = (trend[len(trend)-1]-trend[0]) / trend[0] * 100

        network_clients_trend.labels(node='bridge', country=country).set(value)

write_to_textfile('/srv/metrics.torproject.org/metrics/network/metrics', registry)
